//import path aliases resolver
import 'module-alias/register';

import express from "express";
import path from "path";
import ejsLayout from "express-ejs-layouts";
import { router } from "@controllers/home/HomeRouter";
import { router as aRouter } from "@controllers/service/BasAhu";
import { router as cRouter } from "@controllers/service/BasCh";
import { router as about } from "@controllers/about/AboutRouter";

const server = express();
const port = 3080;

//set static for public driectory
server.use(express.static(__dirname + '/public'));
//set static for ether-m3 lib but permitted access to template directory
server.use([/^\/ether\-m3\/template($|\/)/, '/'], express.static(__dirname + '/lib/ether-m3/dist'));

//set views (template) directory
server.set('views', [path.join(__dirname, '/app/views'), path.join(__dirname, '/lib/ether-m3/dist/ether-m3/template')]);
//set view engine to ejs
server.set('view engine', 'ejs');

//use express-ejs-layouts middleware to render template
server.use(ejsLayout);

//home routing handler
server.use('/', router);

//ahu routing handler
server.use('/', aRouter);

//chiller routing handler
server.use('/', cRouter);

//about router
server.use('/about',about);

//404 not found
server.use('/', (req, res) => {
    res.send("404 not found");

});

server.listen(port, () => {
    console.log(`Server running (http://localhost:${port})`);
});