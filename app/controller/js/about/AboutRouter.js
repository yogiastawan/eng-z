"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const express_1 = __importDefault(require("express"));
exports.router = express_1.default.Router();
exports.router.get('/', (req, res, next) => {
    const local = {
        layout: 'layout',
        titlePage: 'Eng Building Automation System',
        css: ["/ether-m3/css/ether-m3.css",
            "/ether-m3/css/theme/theme-default.css",
            "/css/pages/about.css"
        ]
    };
    res.render('pages/about', local);
});
