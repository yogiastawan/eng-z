import express from "express";
import proxy from "express-http-proxy"

export const router = express.Router();

router.get('/', (req, res, next) => {
    //if already login->show dashboard
        const local = {
            layout: 'layout',
            titlePage: 'Eng Building Automation System',
            css: ["/ether-m3/css/ether-m3.css",
                "/ether-m3/css/theme/theme-default.css",
                "/css/pages/home.css"
            ]
        };
        //else show login page
        res.render('pages/home', local);
   
});

// router.get('/login',(req,res)=>{

// })