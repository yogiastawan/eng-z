import express from "express";

export const router = express.Router();

router.get('/', (req, res, next) => {
    //if already login->show dashboard
        const local = {
            layout: 'layout',
            titlePage: 'Eng Building Automation System',
            css: ["/ether-m3/css/ether-m3.css",
                "/ether-m3/css/theme/theme-default.css",
                "/css/pages/about.css"
            ]
        };
        //else show login page
        res.render('pages/about', local);
   
});

// router.get('/login',(req,res)=>{

// })