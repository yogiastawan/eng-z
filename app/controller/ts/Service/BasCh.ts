import exp from "express";

export const router = exp.Router();

router.get('/chill', (_req, res) => {
    res.status(301).redirect('http://chiller.hopto.org:4061');
});