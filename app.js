"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("module-alias/register");
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const express_ejs_layouts_1 = __importDefault(require("express-ejs-layouts"));
const HomeRouter_1 = require("@controllers/home/HomeRouter");
const BasAhu_1 = require("@controllers/service/BasAhu");
const BasCh_1 = require("@controllers/service/BasCh");
const AboutRouter_1 = require("@controllers/about/AboutRouter");
const server = (0, express_1.default)();
const port = 3080;
server.use(express_1.default.static(__dirname + '/public'));
server.use([/^\/ether\-m3\/template($|\/)/, '/'], express_1.default.static(__dirname + '/lib/ether-m3/dist'));
server.set('views', [path_1.default.join(__dirname, '/app/views'), path_1.default.join(__dirname, '/lib/ether-m3/dist/ether-m3/template')]);
server.set('view engine', 'ejs');
server.use(express_ejs_layouts_1.default);
server.use('/', HomeRouter_1.router);
server.use('/', BasAhu_1.router);
server.use('/', BasCh_1.router);
server.use('/about', AboutRouter_1.router);
server.use('/', (req, res) => {
    res.send("404 not found");
});
server.listen(port, () => {
    console.log(`Server running (http://localhost:${port})`);
});
